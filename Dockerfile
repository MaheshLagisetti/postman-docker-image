FROM postman/newman:alpine

RUN npm install --global newman-reporter-html
RUN npm install --global newman-reporter-htmlextra
